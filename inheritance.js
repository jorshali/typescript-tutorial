var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var AbstractQuote = (function () {
    function AbstractQuote() {
        this._quoteList = [];
    }
    AbstractQuote.prototype.quoteForDisplay = function () {
        var quoteForDisplay = "";
        for (var _i = 0, _a = this._quoteList; _i < _a.length; _i++) {
            var quote = _a[_i];
            quoteForDisplay += this.quoteFormat(quote);
        }
        return quoteForDisplay;
    };
    return AbstractQuote;
}());
var SingleCharacterQuote = (function (_super) {
    __extends(SingleCharacterQuote, _super);
    function SingleCharacterQuote(character, text) {
        _super.call(this);
        this._quoteList.push([character, text]);
    }
    SingleCharacterQuote.prototype.quoteFormat = function (quote) {
        return quote[0] + ": " + quote[1];
    };
    return SingleCharacterQuote;
}(AbstractQuote));
var MultiCharacterQuote = (function (_super) {
    __extends(MultiCharacterQuote, _super);
    function MultiCharacterQuote(quoteList) {
        _super.call(this);
        this._quoteList = quoteList;
    }
    MultiCharacterQuote.prototype.quoteFormat = function (quote) {
        return quote[0] + ": " + quote[1];
    };
    return MultiCharacterQuote;
}(AbstractQuote));
function showQuote(quote) {
    document.body.innerHTML += "<p>" + quote.quoteForDisplay() + "</p>";
}
showQuote(new SingleCharacterQuote("Bobby Boucher", "That's some high quality H2O"));
showQuote(new MultiCharacterQuote([["Coach", "Gatorade!"], ["Bobby Boucher", "You're drinking the wrong water!"]]));
