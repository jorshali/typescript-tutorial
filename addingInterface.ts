interface QuoteInterface {
    character: string;
    text: string;
}

class Quote implements QuoteInterface {
    quotedText: string;

    constructor(public character: string, public text: string) {
        this.quotedText = character + ": " + text;
    }
}

function showQuote(quote: QuoteInterface) {
    document.body.innerHTML += "<h2>" + quote.character + "</h2>";
    document.body.innerHTML += "<ul><li>" + quote.text + "</li></ul>";
}

showQuote(new Quote("Bobby Boucher", "That's some high quality H2O"));