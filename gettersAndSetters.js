var Quote = (function () {
    function Quote() {
        this.updateQuotedText();
    }
    Quote.prototype.updateQuotedText = function () {
        this.quotedText = this._character + ": " + this._text;
    };
    Object.defineProperty(Quote.prototype, "character", {
        get: function () {
            return this._character;
        },
        set: function (newCharacter) {
            this._character = newCharacter;
            this.updateQuotedText();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Quote.prototype, "text", {
        get: function () {
            return this._text;
        },
        set: function (newText) {
            this._text = newText;
            this.updateQuotedText();
        },
        enumerable: true,
        configurable: true
    });
    return Quote;
}());
function showQuote(quote) {
    document.body.innerHTML += "<h2>Favorite Movie Quote</h2>";
    document.body.innerHTML += "<ul><li>" + quote.quotedText + "</li></ul>";
}
var quote = new Quote();
showQuote(quote);
quote.character = "Bobby Boucher";
showQuote(quote);
quote.text = "Captain Insano shows no mercy";
showQuote(quote);
