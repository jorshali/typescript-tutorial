class Quote {
    private _character: string;
    private _text: string;
    public quotedText: string;

    constructor() {
        this.updateQuotedText();
    }

    private updateQuotedText() {
        this.quotedText = this._character + ": " + this._text;
    }

    get character(): string {
        return this._character;
    }

    set character(newCharacter: string) {
        this._character = newCharacter;
        this.updateQuotedText();
    }

    get text(): string {
        return this._text;
    }

    set text(newText: string) {
        this._text = newText;
        this.updateQuotedText();
    }
}

function showQuote(quote: Quote) {
    document.body.innerHTML += "<h2>Favorite Movie Quote</h2>";
    document.body.innerHTML += "<ul><li>" + quote.quotedText + "</li></ul>";
}

let quote: Quote = new Quote();

showQuote(quote);

quote.character = "Bobby Boucher";

showQuote(quote);

quote.text = "Captain Insano shows no mercy";

showQuote(quote);