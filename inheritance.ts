abstract class AbstractQuote {
    protected _quoteList: [string, string][] = [];

    constructor() {}

    quoteForDisplay() {
        let quoteForDisplay: string = "";

        for(let quote of this._quoteList) {
            quoteForDisplay += this.quoteFormat(quote);
        }

        return quoteForDisplay;
    }

    abstract quoteFormat(quote: [string, string]): string;
}

class SingleCharacterQuote extends AbstractQuote {
    constructor(character: string, text: string) {
        super();
        this._quoteList.push([character, text]);
    }

    quoteFormat(quote: [string, string]) {
        return quote[0] + ": " + quote[1];
    }
}

class MultiCharacterQuote extends AbstractQuote {
    constructor(quoteList: [string, string][]) {
        super();
        this._quoteList = quoteList;
    }

    quoteFormat(quote: [string, string]) {
        return quote[0] + ": " + quote[1];
    }
}

function showQuote(quote: AbstractQuote) {
    document.body.innerHTML += "<p>" + quote.quoteForDisplay() + "</p>";
}

showQuote(new SingleCharacterQuote("Bobby Boucher", "That's some high quality H2O"));
showQuote(new MultiCharacterQuote([["Coach", "Gatorade!"],
    ["Bobby Boucher", "You're drinking the wrong water!"]]));

function getMessage(init: boolean) {
    if (init) {
        let message = "Weird";
    }
    return message;
}