var Quote = (function () {
    function Quote(character, text) {
        this.character = character;
        this.text = text;
        this.quotedText = character + ": " + text;
    }
    return Quote;
}());
function showQuote(quote) {
    document.body.innerHTML += "<h2>" + quote.character + "</h2>";
    document.body.innerHTML += "<ul><li>" + quote.text + "</li></ul>";
}
showQuote(new Quote("Bobby Boucher", "That's some high quality H2O"));
showQuote({
    character: "Bobby Boucher",
    text: "That's some high quality H2O"
});
